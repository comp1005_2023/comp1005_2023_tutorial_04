/*
 * https://www.w3resource.com/c-programming-exercises/string/c-string-exercise-10.php
 * This solution only gives the first character with highest frequency.
 * Try to write your own code to show all characters with highest frequency.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define str_size 100 /* Declare the maximum size of the string */
#define chr_no 255 /* Maximum number of characters to be allowed */

int main() {
    char str[str_size];
    int ch_fre[chr_no];
    int i, max, j = 0;
    int ascii;

    printf("\n\nFind maximum occurring character in a string :\n");
    printf("--------------------------------------------------\n");
    printf("Input the string : ");
    fgets(str, sizeof str, stdin);

    for (i = 0; i < chr_no; i++)  /* Set frequency of all characters to 0 */
    {
        ch_fre[i] = 0;
    }

    /* Read for frequency of each characters */
    i = 0;
    max = 0;
    while (str[i] != '\0') {
        ascii = (int) str[i];
        ch_fre[ascii] += 1;
        if(ch_fre[ascii] > max) {
            max = ch_fre[ascii];
        }
        i++;
    }

    printf("The character(s) with highest frequency: ");
    for (i = 0; i < chr_no; i++) {
        if (i != 32 && i != 10) {
            if (ch_fre[i] == max) {
                if(j > 0) {
                    printf(", ");
                }
                printf("\'%c\'", i);
                j++;
            }
        }
    }
    printf("\nFrequency: %d\n\n", max);

    return 0;
}
