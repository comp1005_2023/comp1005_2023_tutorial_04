COMP1005 2023 Tutorial 4
========================


## Background

* [Strings: Part A](https://www.learn-c.org/en/Strings)
* [Strings: Part B](https://www.tutorialspoint.com/cprogramming/c_strings.htm)

## Exercises

0. Write a program in C to input a string and print it.  
    ***Expected Output:*** 
    ```
    Input the string : Welcome, COMP1005 2022!  
    
    The string you entered is : Welcome, COMP1005 2022!  
    ```
    
0. Write a program in C to separate the individual characters from a string.  
    ***Expected Output:*** 
    ```
    Input the string : Welcome, COMP1005 2022!  
    
    The string you entered is : W e l c o m e ,   C O M P 1 0 0 5   2 0 2 0 !  
    ```

0. Write a program in C to extract a substring from a given string.  
    ***Expected Output:*** 
    ```
    Input the string : Welcome, COMP1005 2022!  
    Input the position to start extraction : 10 
    Input the length of substring : 8
    
    The substring is : COMP1005  
    ```

0. Write a program in C that reads a forename, surname and year of birth and display the names and the year one after another sequentially.  
    ***Expected Output:*** 
    ```
    Input your firstname: Tom 
    Input your lastname: Davis 
    Input your year of birth: 1982 
    
    Tom Davis 1982  
    ```

## Extras

0. Write a program in C to print individual characters of string in reverse order.  
    ***Expected Output:*** 
    ```
    Input the string : Welcome, COMP1005 2022!  
    
    The characters of the string in reverse are: ! 0 2 0 2   5 0 0 1 P M O C   , e m o c l e W  
    ```

0. Write a program in C to count total number of alphabets, digits and special characters in a string.  
    ***Expected Output:*** 
    ```
    Input the string : Welcome, COMP1005 2022!  

    Number of Alphabets in the string is : 11 
    Number of Digits in the string is : 8 
    Number of Other characters in the string is : 2
    ```

0. Write a program in C to find maximum occurring character in a string.  
    ***Expected Output:*** 
    ```
    Input the string : Welcome, COMP1005 2022!!!!  

    The character(s) with highest frequency: '!', '0'
    Frequency: 4 
    ```

